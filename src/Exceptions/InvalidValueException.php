<?php

declare(strict_types=1);

namespace App\Exceptions;

use Throwable;
use JetBrains\PhpStorm\Pure;

class InvalidValueException extends \ValueError
{

    /**
     * @param object $object
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    #[Pure]
    public function __construct(
        private object $object,
        string $message = "",
        int $code = 0,
        ?Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return object
     */
    public function getObject(): object
    {
        return $this->object;
    }
}
