<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Track;
use App\Exceptions\InvalidValueException;
use JetBrains\PhpStorm\Pure;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        try {
            $result = $this->getCarsInfo($track) . $this->generateTable($track);
        } catch (InvalidValueException $e) {
            $result = $e->getMessage();
        }
        return $result;
    }

    /**
     * @param Track $track
     * @return string
     */
    private function generateTable(Track $track): string
    {
        $openTag = '<table border="1"><caption>Race results</caption>';
        $tableHead = $this->getTableContent($track);
        return $openTag . $tableHead . '</table>';
    }

    /**
     * @param Track $track
     * @return string
     */
    private function getTableContent(Track $track): string
    {
        $carMethods = get_class_methods(\App\Task1\Car::class);
        $carGetters = preg_grep('#get[A-Za-z]+#', $carMethods);
        $propertyNames = preg_replace('#get([A-Z][a-zA-Z]+)#', '$1', $carGetters);

        $tableHead = '<tr>';
        foreach ($propertyNames as $propertyName) {
            $tableHead .= "<th>$propertyName</th>";
        }
        $tableHead = $tableHead . '</th>';

        $carsGraduated = $track->getCarsGraduated();
        $tableBody = '';
        foreach ($carsGraduated as $car) {
            $tableRow = '';
            foreach ($carGetters as $carGetter) {
                if ($carGetter === 'getImage') {
                    $tableRow .= "<td><img src=\"{$car->$carGetter()}\"></td>";
                } else {
                    $tableRow .= "<td>{$car->$carGetter()}</td>";
                }
            }
            $tableBody .= "<tr>$tableRow</tr>";
        }

        return $tableHead . $tableBody;
    }

    #[Pure]
    public function getCarsInfo(Track $track): string
    {
        $carsInfo = '';
        foreach ($track->all() as $car) {
            $carsInfo .= "{$car->getName()}: {$car->getSpeed()}, {$car->getPitStopTime()}, {$car->getFuelConsumption()}, {$car->getFuelTankVolume()}; ";
        }
        return $carsInfo;
    }
}
