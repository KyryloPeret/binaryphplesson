<?php

declare(strict_types=1);

namespace App\Task1;

use App\Exceptions\InvalidValueException;

class Car
{
    /**
     * @param int $id
     * @param string $image
     * @param string $name
     * @param int $speed
     * @param int $pitStopTime
     * @param float $fuelConsumption
     * @param float $fuelTankVolume
     *
     * @throws InvalidValueException
     */
    public function __construct(
        private int $id,
        private string $image,
        private string $name,
        private int $speed,
        private int $pitStopTime,
        private float $fuelConsumption,
        private float $fuelTankVolume
    ) {
        $this->validate();
    }

    /**
     * @return void
     * @throws InvalidValueException
     */
    public function validate(): void
    {
        if ($this->getId() <= 0) {
            throw new InvalidValueException($this, 'ID must not equal or be less than zero');
        } elseif ($this->getSpeed() <= 0) {
            throw new InvalidValueException($this, 'Speed must not equal or be less than zero');
        } elseif ($this->getPitStopTime() <= 0) {
            throw new InvalidValueException($this, 'Pit-stop time must not equal or be less than zero');
        } elseif ($this->getFuelConsumption() <= 0) {
            throw new InvalidValueException($this, 'Fuel Consumption must not equal or be less than zero');
        } elseif ($this->getFuelTankVolume() <= 0) {
            throw new InvalidValueException($this, 'Fuel Tank Volume must not equal or be less than zero');
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * @return int
     */
    public function getPitStopTime(): int
    {
        return $this->pitStopTime;
    }

    /**
     * @return float
     */
    public function getFuelConsumption(): float
    {
        return $this->fuelConsumption;
    }

    /**
     * @return float
     */
    public function getFuelTankVolume(): float
    {
        return $this->fuelTankVolume;
    }
}
