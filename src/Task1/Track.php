<?php

declare(strict_types=1);

namespace App\Task1;

use App\Exceptions\InvalidValueException;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class Track
{
    /**
     * @var Car[] @example Car[carId] = car
     */
    private array $cars;

    /**
     * @param float|int $lapLength
     * @param int $lapsNumber
     *
     * @throws InvalidValueException
     */
    public function __construct(
        private float|int $lapLength,
        private int $lapsNumber = 1
    ) {
        $this->validate();
    }

    /**
     * @return void
     * @throws InvalidValueException
     */
    public function validate(): void
    {
        if ($this->getLapLength() <= 0) {
            throw new InvalidValueException($this, 'Lap length equals or less than zero');
        } elseif ($this->getLapsNumber() <= 0) {
            throw new InvalidValueException($this, 'Laps number equals or less than zero');
        }
    }

    /**
     * @return float|int
     */
    public function getLapLength(): float|int
    {
        return $this->lapLength;
    }

    /**
     * @return int
     */
    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    /**
     * @param Car $car
     * @return void
     */
    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    /**
     * @return Car[]
     */
    public function all(): array
    {
        return $this->cars;
    }

    /**
     * @return Car
     */
    public function run(): Car
    {
        $graduatedCars = $this->getCarsGraduated();
        return $graduatedCars[array_key_first($graduatedCars)];
    }

    /**
     * @return Car[]
     */
    public function getCarsGraduated(): array
    {
        $carsWithTrackTime = $this->getCarsWithTrackTime();
        $graduatedCars = $this->getSortedCars($carsWithTrackTime);
        return $this->getCarsWithoutTrackTime($graduatedCars);
    }

    /**
     * @param CarWithTrackTime[] $carsWithTrackTime
     * @return CarWithTrackTime[]
     */
    public function getSortedCars(array $carsWithTrackTime): array
    {
        $callableForSorting = function (
            CarWithTrackTime $carWithTrackTime1,
            CarWithTrackTime $carWithTrackTime2
        ) {
            if ($carWithTrackTime1->getCarTrackTime() === $carWithTrackTime2->getCarTrackTime()) {
                return 0;
            }
            return ($carWithTrackTime1->getCarTrackTime() < $carWithTrackTime2->getCarTrackTime()) ? -1 : 1;
        };

        usort($carsWithTrackTime, $callableForSorting);

        return $carsWithTrackTime;
    }

    /**
     * @param CarWithTrackTime[] $carsWithTrackTime
     * @return Car[]
     */
    #[Pure]
    public function getCarsWithoutTrackTime(array $carsWithTrackTime): array
    {
        $carsWithoutTrackTime = [];
        foreach ($carsWithTrackTime as $carWithTrackTime) {
            $carsWithoutTrackTime[] = $carWithTrackTime->getCar();
        }

        return $carsWithoutTrackTime;
    }

    /**
     * @return CarWithTrackTime[]
     */
    #[Pure]
    public function getCarsWithTrackTime(): array
    {
        $carsWithTrackTime = [];
        foreach ($this->all() as $car) {
            $carsWithTrackTime[] = new CarWithTrackTime($car, $this->getCarTrackTime($car));
        }

        return $carsWithTrackTime;
    }

    /**
     * @return float|int
     */
    #[Pure]
    public function getDistance(): float|int
    {
        return $this->getLapLength() * $this->getLapsNumber();
    }

    #[Pure]
    private function getCarTrackTime(Car $car): float|int
    {
        $fuelForTrack = $this->getDistance() / 100 * $car->getFuelConsumption();
        $pitStopCount = floor($fuelForTrack / $car->getFuelTankVolume());
        $timeForPitStop = $pitStopCount * $car->getPitStopTime();
        $raceTimeSeconds = ($this->getDistance() / $car->getSpeed()) * 60 * 60;

        return $raceTimeSeconds + $timeForPitStop;
    }
}
