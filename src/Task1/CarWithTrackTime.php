<?php

declare(strict_types=1);

namespace App\Task1;

use App\Exceptions\InvalidValueException;

class CarWithTrackTime
{
    /**
     * @param Car $car
     * @param float|int $carTrackTime
     *
     * @throws InvalidValueException
     */
    public function __construct(
        private Car $car,
        private float|int $carTrackTime
    ) {
        $this->validate();
    }

    /**
     * @return void
     * @throws InvalidValueException
     */
    public function validate(): void
    {
        if ($this->getCarTrackTime() <= 0) {
            throw new InvalidValueException($this, 'Price must not equal or be less than zero');
        }
    }

    /**
     * @return Car
     */
    public function getCar(): Car
    {
        return $this->car;
    }

    /**
     * @return float|int
     */
    public function getCarTrackTime(): float|int
    {
        return $this->carTrackTime;
    }
}
