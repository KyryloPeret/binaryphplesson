<?php

declare(strict_types=1);

namespace App\Task2;

use App\Exceptions\InvalidValueException;

class BooksGenerator
{
    /**
     * @param int $minPagesNumber
     * @param array $libraryBooks
     * @param int $maxPrice
     * @param array $storeBooks
     *
     * @throws InvalidValueException
     */
    public function __construct(
        private int $minPagesNumber,
        private array $libraryBooks,
        private int $maxPrice,
        private array $storeBooks
    ) {
        $this->validate();
    }

    /**
     * @return void
     * @throws InvalidValueException
     */
    public function validate(): void
    {
        if ($this->getMinPagesNumber() <= 0) {
            throw new InvalidValueException($this, 'Min Pages Number must not equal or be less than zero');
        } elseif ($this->getMaxPrice() <= 0) {
            throw new InvalidValueException($this, 'Max Price must not equal or be less than zero');
        }
    }

    /**
     * @return int
     */
    public function getMinPagesNumber(): int
    {
        return $this->minPagesNumber;
    }

    /**
     * @return Book[]
     */
    public function getLibraryBooks(): array
    {
        return $this->libraryBooks;
    }

    /**
     * @return int
     */
    public function getMaxPrice(): int
    {
        return $this->maxPrice;
    }

    /**
     * @return Book[]
     */
    public function getStoreBooks(): array
    {
        return $this->storeBooks;
    }

    /**
     * @return \Generator
     */
    public function generate(): \Generator
    {
        foreach ($this->getLibraryBooks() as $libraryBook) {
            if ($libraryBook->getPagesNumber() >= $this->getMinPagesNumber()) {
                yield $libraryBook;
            }
        }
        foreach ($this->getStoreBooks() as $storeBook) {
            if ($storeBook->getPrice() <= $this->getMaxPrice()) {
                yield $storeBook;
            }
        }
    }
}
