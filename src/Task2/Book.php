<?php

declare(strict_types=1);

namespace App\Task2;

use App\Exceptions\InvalidValueException;

class Book
{
    /**
     * @param string $title
     * @param int $price
     * @param int $pagesNumber
     *
     * @throws InvalidValueException
     */
    public function __construct(
        private string $title,
        private int $price,
        private int $pagesNumber
    ) {
        $this->validate();
    }

    /**
     * @return void
     * @throws InvalidValueException
     */
    public function validate(): void
    {
        if ($this->getPrice() <= 0) {
            throw new InvalidValueException($this, 'Price must not equal or be less than zero');
        } elseif ($this->getPagesNumber() <= 0) {
            throw new InvalidValueException($this, 'Pages Number must not equal or be less than zero');
        }
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}
